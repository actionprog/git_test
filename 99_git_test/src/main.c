/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 * This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * Copyright (c) 2017 STMicroelectronics International N.V.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted, provided that the following conditions are met:
 *
 * 1. Redistribution of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of STMicroelectronics nor the names of other
 *    contributors to this software may be used to endorse or promote products
 *    derived from this software without specific written permission.
 * 4. This software, including modifications and/or derivative works of this
 *    software, must execute solely and exclusively on microcontroller or
 *    microprocessor devices manufactured by or for STMicroelectronics.
 * 5. Redistribution and use of this software other than as permitted under
 *    this license is void and will automatically terminate your rights under
 *    this license.
 *
 * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
 * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT
 * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l1xx_hal.h"
#include "cmsis_os.h"

#define	_UseUartDma				0
#define	_UseUart1DmaIntrrupt	0
#if	(0 == _UseUart1DmaIntrrupt)
#define	_UseUart1ThroughUart2	0
#endif

uint8_t ucUart1DmaRx = 0;
uint32_t ulUart1DmaRxCnt = 0;

#define	_UseAdc					1
#define	_UseAdcDma				1
#define _UseAdcDmaInterrupt		0

uint16_t usAdcDmaValue;

#define	_UseDAC					1
#define	_UseDacDMA				1
#define	_UseDacDmaInterrupt		0

uint16_t usDacDmaValue;


/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;
DMA_HandleTypeDef hdma_adc;

COMP_HandleTypeDef hcomp2;

TIM_HandleTypeDef htim6;

DAC_HandleTypeDef hdac;
DMA_HandleTypeDef hdma_dac_ch1;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart1_rx;

osThreadId defaultTaskHandle;
osThreadId myTask02Handle;
osMessageQId myQueue01Handle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_ADC_Init(void);
static void MX_DAC_Init(void);
static void MX_TIM6_Init(void);
static void MX_COMP2_Init(void);

void StartDefaultTask(void const * argument);
void StartTask02(void const * argument);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
#if	(1 == _UseAdc)
	MX_ADC_Init();
#endif

#if	(1 == _UseDAC)
#if	(1 == _UseDacDMA)
	MX_TIM6_Init();
	HAL_TIM_Base_Start(&htim6);
//	HAL_TIM_Base_Start_IT(&htim6);
#endif
	MX_DAC_Init();
#endif

	MX_COMP2_Init();
	HAL_COMP_Start(&hcomp2);

#if	(1 == _UseUartDma)
	printf("UART DMA TEST - actionprog\n");
#endif
#if	(1 == _UseAdc)
	printf("ADC TEST - actionprog\n");
#endif

	/* USER CODE BEGIN 2 */

	/* USER CODE END 2 */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* Create the thread(s) */
	/* definition and creation of defaultTask */
	osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 256);
	defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

	/* definition and creation of myTask02 */
	osThreadDef(myTask02, StartTask02, osPriorityNormal, 0, 256);
	myTask02Handle = osThreadCreate(osThread(myTask02), NULL);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* Create the queue(s) */
	/* definition and creation of myQueue01 */
	osMessageQDef(myQueue01, 2, uint16_t);
	myQueue01Handle = osMessageCreate(osMessageQ(myQueue01), NULL);

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */


	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
 */
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
	RCC_OscInitStruct.PLL.PLLDIV = RCC_PLL_DIV3;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

	huart2.Instance = USART2;
	huart2.Init.BaudRate = 115200;
	huart2.Init.WordLength = UART_WORDLENGTH_8B;
	huart2.Init.StopBits = UART_STOPBITS_1;
	huart2.Init.Parity = UART_PARITY_NONE;
	huart2.Init.Mode = UART_MODE_TX_RX;
	huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart2.Init.OverSampling = UART_OVERSAMPLING_16;
	if (HAL_UART_Init(&huart2) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

}

/** 
 * Enable DMA controller clock
 */
static void MX_DMA_Init(void) 
{
	/* DMA controller clock enable */
	__HAL_RCC_DMA1_CLK_ENABLE();

#if	(1 == _UseUartDma)
#if	(1 == _UseUart1DmaIntrrupt)
	/* DMA interrupt init */
	/* DMA1_Channel5_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 5, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
#endif
#endif

#if	(1 == _UseAdcDma)
#if	(1 == _UseAdcDmaInterrupt)
	  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 5, 0);
	  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
#endif
#endif

#if	(1 == _UseDacDMA)
#if	(1 == _UseDacDmaInterrupt)
	  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 5, 0);
	  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
#endif
//	  HAL_NVIC_SetPriority(TIM6_IRQn, 2, 0);
//	  HAL_NVIC_EnableIRQ(TIM6_IRQn);
#endif

}

/* ADC init function */
static void MX_ADC_Init(void)
{
	ADC_ChannelConfTypeDef sConfig;

	/**Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
	 */
	hadc.Instance = ADC1;
	hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
	hadc.Init.Resolution = ADC_RESOLUTION_12B;
	hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	hadc.Init.ScanConvMode = ADC_SCAN_DISABLE;
	hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	hadc.Init.LowPowerAutoWait = ADC_AUTOWAIT_DISABLE;
	hadc.Init.LowPowerAutoPowerOff = ADC_AUTOPOWEROFF_DISABLE;
	hadc.Init.ChannelsBank = ADC_CHANNELS_BANK_A;
	hadc.Init.ContinuousConvMode = ENABLE;
	hadc.Init.NbrOfConversion = 1;
	hadc.Init.DiscontinuousConvMode = DISABLE;
	hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
	hadc.Init.DMAContinuousRequests = ENABLE;
	if (HAL_ADC_Init(&hadc) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time.
	 */
	sConfig.Channel = ADC_CHANNEL_0;
	sConfig.Rank = 1;
	sConfig.SamplingTime = ADC_SAMPLETIME_4CYCLES;
	if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

/* DAC init function */
static void MX_DAC_Init(void)
{
	DAC_ChannelConfTypeDef sConfig;

	/**DAC Initialization
	 */
	hdac.Instance = DAC;
	if (HAL_DAC_Init(&hdac) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**DAC channel OUT1 config
	 */
#if	(1 == _UseDacDMA)
	sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_DISABLE;
#else
	sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_DISABLE;
#endif
	if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

static void MX_TIM6_Init(void)
{
	TIM_MasterConfigTypeDef sMasterConfig;

	htim6.Instance = TIM6;
	htim6.Init.Prescaler = 0xFFFF;
	htim6.Init.Period = 0x200;
	htim6.Init.ClockDivision = 0;
	htim6.Init.CounterMode = TIM_COUNTERMODE_UP;

	if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

static void MX_COMP2_Init(void)
{
	hcomp2.Instance = COMP2;
	hcomp2.Init.InvertingInput = COMP_INVERTINGINPUT_VREFINT;
	hcomp2.Init.NonInvertingInput = COMP_NONINVERTINGINPUT_PB4;
	hcomp2.Init.Output = COMP_OUTPUT_NONE;
	hcomp2.Init.Mode = COMP_MODE_LOWSPEED;
	hcomp2.Init.WindowMode = COMP_WINDOWMODE_DISABLE;
	hcomp2.Init.TriggerMode = COMP_TRIGGERMODE_NONE;
	if (HAL_COMP_Init(&hcomp2) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
{

	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pin : B1_Pin */
	GPIO_InitStruct.Pin = B1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : LD2_Pin */
	GPIO_InitStruct.Pin = LD2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* StartDefaultTask function */
void StartDefaultTask(void const * argument)
{
	uint32_t ulTemp = 0;
	uint8_t ucTemp = 0;
	/* USER CODE BEGIN 5 */
	/* Infinite loop */
	for(;;)
	{
		osDelay(500);
#if	(1 == _UseUartDma)
		{
#if	(1 == _UseUart1DmaIntrrupt)
			ulTemp = ulUart1DmaRxCnt;

			printf("Dma - %ld\n", ulTemp);
#else
#if	(0 == _UseUart1ThroughUart2)
			ucTemp = ucUart1DmaRx;
			HAL_UART_Transmit(&huart2, &ucTemp, 1, 100);
#endif
#endif
		}
#endif
	}
	/* USER CODE END 5 */
}

/* StartTask02 function */
void StartTask02(void const * argument)
{
	/* USER CODE BEGIN StartTask02 */
	/* Infinite loop */
#if	(1 == _UseAdc)
#if	(1 == _UseAdcDma)
#if	(1 == _UseAdcDmaInterrupt)
	printf("ADC DMA Start Interrupt\n");
	HAL_ADC_Start_DMA(&hadc, &usAdcDmaValue, 1);
#else
	printf("ADC DMA Start\n");
	HAL_DMA_Start(&hdma_adc, &hadc.Instance->DR, &usAdcDmaValue, 1);
	hadc.Instance->CR2 |= ADC_CR2_DMA;
#endif
	printf("ADC Start\n");
	HAL_ADC_Start(&hadc);
#endif
#endif

#if	(1 == _UseUartDma)
#if	(1 == _UseUart1DmaIntrrupt)
	HAL_UART_Receive_DMA(&huart1, &ucUart1DmaRx, 1);
#else
#if	(1 == _UseUart1ThroughUart2)
	HAL_DMA_Start(&hdma_usart1_rx, &huart1.Instance->DR, &huart2.Instance->DR, 1);
#else
	HAL_DMA_Start(&hdma_usart1_rx, &huart1.Instance->DR, &ucUart1DmaRx, 1);
#endif
	SET_BIT(huart1.Instance->CR3, USART_CR3_DMAR);
#endif
#endif

#if	(1 == _UseDAC)
#if	(1 == _UseDacDMA)
#if	(1 == _UseDacDmaInterrupt)
//	DAC_ChannelConfTypeDef xconf;
//	const uint16_t aEscalator8bit[] = {0x0, 0x33, 0x66, 0x99, 0xCC, 0xFF};
//	xconf.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
//	xconf.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
//	HAL_DAC_ConfigChannel(&hdac, &xconf, DAC_CHANNEL_1);
//	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)aEscalator8bit, 6, DAC_ALIGN_8B_R);

	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t *)&usDacDmaValue, 1, DAC_ALIGN_12B_R);
#else
	usDacDmaValue = 100;

	printf("DAC DMA Start - %d\n", usDacDmaValue);
	SET_BIT(hdac.Instance->CR, DAC_CR_DMAEN1);			//	Enable the selected DAC channel1 DMA request
	HAL_DMA_Start(&hdma_dac_ch1, (uint32_t)&usDacDmaValue, (uint32_t)&hdac.Instance->DHR12R1, 1);
	__HAL_DAC_ENABLE(&hdac, DAC_CHANNEL_1);				//	Enable the Peripharal
#endif
#else
	HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, usDacDmaValue);
	HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
#endif
#endif
	for(;;)
	{
		printf(" \n");
#if	(1 == _UseAdc)
#if	(1 == _UseAdcDma)
#if	(1 == _UseAdcDmaInterrupt)
		printf("ADC DMA - %d\n", usAdcDmaValue);
#else
		printf("ADC DMA - %d\n", usAdcDmaValue);
#endif
#endif
#endif

#if	(1 == _UseDAC)
		if(usDacDmaValue == hdac.Instance->DOR1)
			usDacDmaValue += 100;
		if(0x0FFF < usDacDmaValue)
			usDacDmaValue = 100;
//		usDacDmaValue = 0x0FFF;
		printf("DAC Value - %d\n", usDacDmaValue);
		printf("DAC Holding - %x\n", hdac.Instance->DHR12R1);
		printf("DAC Output - %x\n", hdac.Instance->DOR1);
//		printf("DAC CR - %lx\n", hdac.Instance->CR);
//		printf("DAC SR - %lx\n", hdac.Instance->SR);
#if	(1 == _UseDacDMA)
//		SET_BIT(hdac.Instance->SWTRIGR, DAC_SWTRIGR_SWTRIG1);
#if	(1 == _UseDacDmaInterrupt)
#else
#endif
#else
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, usDacDmaValue);
#endif
#endif

		printf("COMP2 - %d\n", HAL_COMP_GetOutputLevel(&hcomp2));

		osDelay(500);
	}
	/* USER CODE END StartTask02 */
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void _Error_Handler(char * file, int line)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	printf("Error in %s - %d\n", file, line);
	while(1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */

}

#endif

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	_Led2Toggle();
	ulUart1DmaRxCnt++;

}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	_Led2Toggle();
}

void HAL_DAC_ConvCpltCallbackCh1(DAC_HandleTypeDef* hdac)
{
	_Led2Toggle();
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	_Led2Toggle();
}

PUTCHAR_PROTOTYPE
{
	uint8_t ucTemp = '\r';
	/* Place your implementation of fputc here */
	/* e.g. write a character to the USART2 and Loop until the end of transmission */
	if('\n' == ch)
		HAL_UART_Transmit(&huart2, &ucTemp, 1, 0xFFFF);
	HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);

	return ch;
}

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
